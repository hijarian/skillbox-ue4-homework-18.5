#pragma once
#include <deque>

template<typename T>
class MyStack
{
public:
	MyStack<T>() : container(std::deque<T>{}) {};
	T pop() {
		T value = container.back(); // explicit copy!
		container.pop_back(); // container.back() reference is invalid after this!
		return value; // hoping for copy elision to happen here
	};
	void push(T value) {
		container.push_back(value);
	};
	size_t get_size() { return container.size(); };
private:
	std::deque<T> container; // trying to template away the `std::deque` hurt my small brain.
};
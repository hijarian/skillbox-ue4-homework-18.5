#include <iostream>
#include <string>
#include "MyStack.h"

int main()
{
	MyStack<int> int_stack{};
	int_stack.push(1);
	int_stack.push(2);
	int_stack.push(5);

	std::cout << int_stack.pop() << " " << int_stack.pop() << " " << int_stack.pop() << std::endl;

	MyStack<std::string> string_stack{};
	string_stack.push("!");
	string_stack.push("alive");
	string_stack.push("am");
	string_stack.push("I");

	size_t initial_string_stack_size = string_stack.get_size();
	for (size_t i = 0; i < initial_string_stack_size; ++i)
	{
		std::cout << string_stack.pop() << " ";
	}
	std::cout << std::endl;
}